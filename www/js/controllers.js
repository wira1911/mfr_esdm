angular.module('starter.controllers', ['ngSanitize','ionic'])

.directive('modalDialog', function() {
  return {
    restrict: 'E',
    scope: {
      show: '='
    },
    replace: true, // Replace with the template below
    transclude: true, // we want to insert custom content inside the directive
    link: function(scope, element, attrs) {
      scope.dialogStyle = {};
      if (attrs.width)
        scope.dialogStyle.width = attrs.width;
      if (attrs.height)
        scope.dialogStyle.height = attrs.height;
    },
    template: "<div class='ng-modal' ng-show='show'>"+
      "<div class='ng-modal-overlay' ng-click='hideModal()'></div>"+
      "<div class='ng-modal-dialog' ng-style='dialogStyle' style='border-radius:2px;'>"+
        "<div class='ng-modal-dialog-content' ng-transclude></div>"+
      "</div>"+
    "</div>" // See below
  };
})

.controller('LoginCtrl', function($scope, LoginService, $ionicPopup, $state) {
    $scope.data = {};

    $scope.login = function() {
        LoginService.loginUser($scope.data.username, $scope.data.password).success(function(data) {
            $state.go('main.home');
        }).error(function(data) {
            var alertPopup = $ionicPopup.alert({
                title: 'Login failed!',
                template: 'Please check your credentials!'
            });
        });
    }
})

.controller('MainCtrl', function($scope) {})

.controller('HomeCtrl', function($scope) {})

.controller('ProjectsCtrl', function($scope,$ionicModal) {
    /*start modal from templates*/
    $ionicModal.fromTemplateUrl('templates/projectAdd.html', { scope: $scope }).then(function(modal) { $scope.modal = modal; });
    $scope.addProject = function() { $scope.modal.show(); };
    $scope.closeInsert = function() { $scope.modal.hide(); };
    /*end for modal*/
})

.controller('ProjectAddCtrl',function($scope){})

.controller('ProjectDetailCtrl',['$scope','$sce','$sanitize','$ionicPopup',function($scope,$stateParams,$sanitize,$state,$sce,$ionicPopup){

  //var nomorMarker = 0;
    angular.extend($scope,{
        center: {
          //lat:1.10344,
          //lon:104.049468,
          //--lapangan 2 center WMS
          //lat: -3.2934418808696213,
          //lon: 116.02117012626766,
          //--openstreetmap adaro center
            lat: -6.22792666039010090,
            lon: 106.83105468749872000,
            zoom:15
        },
        /*
        defaults:{
          events:{
            map: ['singleclick']
          }
        },
        */
        markers: [],
        wms:{
            source: {
                type: 'ImageWMS',
                //url: 'http://demo.opengeo.org/geoserver/wms',
                url: 'http://localhost:8080/geoserver/it.geosolutions/wms',
                //params: { LAYERS: 'topp:states', tiled:true},
                params: { LAYERS: 'it.geosolutions:2 lapangan', tiled:false },
                serverType: 'geoserver'
            }
        },
        osmlocal: {
            source: {
                type: 'OSM',
                //url: 'http://localhost/OSM_ADARO/${z}/${x}/${y}.png',
                url: 'http://localhost/OSM_ADARO/{z}/{x}/{y}.png',
                //url: 'http://{a-c}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png'
            }
        },
        osmcloud: {
            source: {
                type: 'OSM',
                url: 'http://128.199.111.248/OSM_adaro/osm_adaro/{z}/{x}/{y}.png'
            } 
        },
        mapquest:{
            source:{
                type: 'MapQuest',
                layer:'osm'
            }
        },
        controls:[
            { name: 'zoom', active: true},
            { name: 'fullscreen', active: true}
        ]
    });
    
    
    // $scope.toggleModal = function() {
    //     $scope.modalShown = !$scope.modalShown;
    // };
    var data = {
        proj1:{
            lat: -6.22792666039010090,
            lon: 106.83105468749872000,
        },
        proj2:{
            lat: -6.22408705073551,
            lon: 106.8322563171374,
        }
    };
    var i = 0;
    var arr1=[];
    var json1={};
    $scope.addMarker = function(){
        parseInt(i+=1);
        console.log(i);
        $scope.modalShown = false;
        json1.lat="";
        json1.lon="";
        arr1.push(json1);

        $scope.markers.push({
            //name: nomorMarker + 1,
            lat: data.proj1.lat,
            lon: data.proj2.lon,
            draggable: true,
            label: {
                show: false,
                message: "Reclamation Projects"
            },
            onClick: function (event, properties) {
                $scope.modalShown = !$scope.modalShown;
                console.log(properties);
            }
        });
        $scope.closeModal = function(){
            $scope.modalShown = false;
        };
    };
    $scope.picture = function(){
        navigator.camera.getPicture(cameraSuccess, cameraError, { quality: 50, destinationType: Camera.DestinationType.FILE_URI });
    };
    var cameraSuccess = function(imageURI){
        console.log("camera sukses");
        var image = document.getElementById('myImage');
        image.src = imageURI;
    };
    var cameraError = function(e){
        console.log("error : "+e);
    };
}])

.controller('DashCtrl', function($scope) {})

.controller('ChatsCtrl', function($scope, Chats) {
    $scope.chats = Chats.all();
    $scope.remove = function(chat) {
        Chats.remove(chat);
    }
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
    $scope.chat = Chats.get($stateParams.chatId);
})

.controller('FriendsCtrl', function($scope, Friends) {
    $scope.friends = Friends.all();
})

.controller('FriendDetailCtrl', function($scope, $stateParams, Friends) {
    $scope.friend = Friends.get($stateParams.friendId);
})

.controller('AccountCtrl', function($scope) {
    $scope.settings = {
        enableFriends: true
    };
});
